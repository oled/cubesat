# CubeSat ODU Contributors

## Software Team

Team Lead: [Derek Goddeau](https://gitlab.com/Datenstrom)

-   <dgodd001@odu.edu>
-   Slack: derek.goddeau

&nbsp;

-   [Richard Cornish](https://gitlab.com/rcornish4)

-   [Jacob Hughes](https://gitlab.com/jhugh042)


--------------------------------------------------------------------------------

## ADCS Code Team

-   [Nathan Sivertson](https://gitlab.com/Nsive001)
-   [Stephen Richards](https://gitlab.com/srich046)
-   [Joe Kingett](https://gitlab.com/JoeKingett)

--------------------------------------------------------------------------------


## Past Contributers
