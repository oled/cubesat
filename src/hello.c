/** 
 *  @file   hello.c
 *  @brief  I say hello
 */
#include <stdio.h>
#include <stdlib.h>

/**
 *  @brief this main func
 *  @ return EXIT_SUCCESS
 */
int main()
{
    printf("hello, world\n");
    
    return EXIT_SUCCESS;
}
