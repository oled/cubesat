# Contribution Guide

The goal of the contribution guide is to reduce the friction for
new contributors, and to support the natural life cycle of project
versions from experimental through to stable, by allowing safe
experimentation, rapid failure, and isolation of stable code.
It will reduce the complexity of project repositories and make it
easier for contributers to participate and reduce the scope for
error.

--------------------------------------------------------------------------------

## Git

The project uses git as its distributed version control system, a number
of [graphical git management programs](https://git-scm.com/download/gui/linux)
exist although the command line is the preferred method of using git.
An excellent introduction to git can be found [here](https://www.atlassian.com/git/tutorials/what-is-git).


--------------------------------------------------------------------------------

## General Guidelines

-   Create [issues](https://gitlab.com/Datenstrom/cubesat/issues) with the proper [labels](https://gitlab.com/Datenstrom/cubesat/labels) and move them across the stages on the issue tracker board adding comments about what was done.
-   Check if your code follows the [Coding Conventions](docs/pages/style.md). If the code does not comply these style rules, your code will not be merged.
-   Keep commits to the point, e.g., don't add whitespace/typo fixes to other code changes. If changes are layered, layer the patches.
-   Describe the technical detail of the change(s) as specific as possible.
-   Closing an issue or merge request *must* always be accompanied by a comment indicating why it gets closed.
-   The master branch should always be in a working state. The software team will create release tags based on this branch, whenever a milestone is completed.


--------------------------------------------------------------------------------

## Workflow

-   Use the [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

    -   `master` branch is for official releases only, it should always be in a working state. 
    -   `develop` branch is the working branch, and basically uses the [feature branching workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow).
    -   When a release is approaching and no new features must be added a `release` branch will be forked off `develop`.
    -   No new features will be added to `release` only fixes and documentation.
    -   When `release` is ready it will be merged back into master and tagged with a version number. It will also be merged back into `develop`.
    -   "hotfix" branches may be used to quickly patch production releases, this is the only time a branch should be forked off of `master`.

-   Create a new branch for every new feature or fix, it can be pushed to the repository for backup and collaboration.
-   When a feature or fix is complete merge it into the `develop` branch.
-   Delete your feature branch from the upstream repository after it's merged, unless you have a good reason not to.

To list merged branches while being on `master`:

    $ git branch --merged | grep -v "\*"


### Naming Branches

-   Use _short_ and _descriptive_ names with the issue number.
-   Use dashes to separate words.

For example:

    # good
    $ git checkout -b 42-adcs-pid
    
    # bad
    $ git checkout -b bug_fix

### Commits

-   Each commit should be a single _logical change_. Do not make several _logical changes_ in one commit. For example, if a patch fixes a bug and optimizes the performance of a feature, split it into two separate commits. Use `git add -p` to interactively stage specific
portions of modified files.
-   Don't split a single _logical change_ into several commits. Implementation of a feature and its tests should be in the same commit.  
-   Commit _early_ and _often_. Small, self-contained commits are easier to understand and revert when something goes wrong.

While working alone on a local branch that has not yet been pushed, it's fine to use commits as temporary snapshots of your work. However, it still holds true that you should apply all of the above before pushing it. One way to handle this is to create your own feature branch, commit all you like, and when you are ready use `git merge --squash` to squash the entire branch into one commit. Then just delete the remote feature branch.


### Messages

Use the editor, not the terminal, when writing a commit message:

    # good
    $ git commit

    # bad
    $ git commit -m "Quick fix"

    # very bad
    $ git commit -m "Updated main.c"

Committing from the terminal encourages a mindset of having to fit 
everything in a single line which usually results in non-informative,
ambiguous commit messages. The summary line should be _descriptive_
but concise and no longer than 50 characters. After that should be a
blank line followed by a more thorough description. It should be
wrapped to 72 characters and explain _why_ the change is needed, _how_
it addresses the issue and what _side-effects_ it might have.
It should also provide pointers to related resources (eg. link to the
issue on the bug tracker).

If a commit is going to be squashed into another commit use the `--squash` and
`--fixup` flags respectively to make the intention clear. Use `--autosquash` when
_rebasing_, the marked commits will be squashed automatically.

    $ git commit --squash fd86fbb8

--------------------------------------------------------------------------------
