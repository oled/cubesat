# ODU CubeSat

[![build status](https://gitlab.com/oled/cubesat/badges/master/build.svg)](https://gitlab.com/oled/cubesat/commits/master)
[![coverage](http://gitlab.com/oled/cubesat/badges/master/coverage.svg?job=coverage)](https://gitlab.com/oled/cubesat/commits/master)

## Overview

ODU CubeSat project Software Requirements Specification (SRS) and Concept of
Operations (CONOPS) Documentation.

Project structure:

    |-- docs
    |   |-- conops
    |   |   `-- CONOPS LaTeX source files
    |   |-- doxygen
    |   |   `-- Doxygen output
    |   |-- pages
    |   |   `-- Markdown documentation files for Doxygen
    |   |-- srs
    |   |   `-- Software Requirements Specification LaTeX source files
    |   |-- Doxyfile
    |   |-- Doxygen formating html & css
    |-- src
    |   `-- Program source code
    `-- README.md



## Documentation

All instructions assume a Debian Linux based operating system.


### Doxygen

Install Doxygen:

    apt-get install doxygen

Run doxygen using the config file from the project root:

    doxygen docs/Doxyfile


### Building LaTeX Documentation



    apt-get install texlive-full
    pdflatex -output-directory docs/srs/pdf docs/srs/main.tex
