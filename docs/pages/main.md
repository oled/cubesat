# CubeSat ODU  {#mainpage}

The ODU cubesat is one of three satellites in the Undergraduate Student
Instrument Program (USIP) Virginia Constellation, in collaboration with
UVA, VT and HU. Our objective is to more accurately determine the effect of
atmospheric drag in Low Earth Orbit (LEO) and validate a novel inflatable
drag brake. This is ODU's first cubesat, and the first steps into our fledgling
space program.

We will collect Global Positioning
System (GPS) and accelerometer data throughout the flight. Using the collected 
data, we will be able to infer the drag on the satellite in both normal
and deployed states. We will also communicate with other satellites in the
constellation to ensure full data recovery and perform ranging experiments
to validate the GPS data.


Check out the [team](AUTHORS.md).

## References

-   See the [CONOPS](https://gitlab.com/Datenstrom/cubesat/raw/master/docs/conops/ConOps.pdf) for more about the project.
-   See the [SRS](https://gitlab.com/Datenstrom/cubesat/raw/master/docs/srs/pdf/main.pdf) for more about the software.

--------------------------------------------------------------------------------

## How can I contribute to the project?

-   Follow the [contribution guide](CONTRIBUTING.md).
-   Follow the [style guide](docs/pages/style.md).

--------------------------------------------------------------------------------

## Research Documentation

-   Why C and not C++?

    -   The C language has a larger community for embedded projects.
    -   The C language will provide more flexibility in future ODU space missions
        for computer engineers. This is because it is more portable than C++
        between embedded architectures. For example not all functionality of
        C++ is available out of the box on AVR.

-   Operating Systems Research

    -   Possible solutions for [multiprocessing](docs/pages/processing/processing.md).
    -   Possible solutions for [data storage](docs/pages/data.md).

--------------------------------------------------------------------------------

## Resources

-   C Language Resources

    -   See the [style guide](docs/pages/style.md).
    -   Order a copy of the [C holy scripture](https://en.wikipedia.org/wiki/The_C_Programming_Language) by the prophets Kernighan and Ritchie.
    -   Read and understand the [Ten Commandments of C Programming](http://www.lysator.liu.se/c/ten-commandments.html)
    -   Read, bookmark, memorize [How to C in 2016](https://matt.sh/howto-c).
    -   Read the [C FAQ](http://c-faq.com/)

-   Multiprocessing Solution Resources

    -   [Case study on implementing SMP in FreeRTOS](https://www.cs.york.ac.uk/fp/multicore-freertos/spe-mistry.pdf)
    -   [Example of FreeRTOS ported to SMP on Pandaboard](https://github.com/n-aizu/freertos-multicore)

-   Navigation Systems Resources

    -   [Dilution of Precision](https://en.wikipedia.org/wiki/Dilution_of_precision_%28navigation%29)
    -   [GPS data point compression](https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm)
    -   [Writing GPS Applications](http://www.codeproject.com/Articles/9115/Writing-Your-Own-GPS-Applications-Part)
