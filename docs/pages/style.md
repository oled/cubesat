# Coding Conventions

The goal of the coding conventions are to give the code the appearance of a
single perfect author writing at one moment in time, and to Reduce the
number of lines of code and complexity of C projects. The results should be
that:

-   The code is easy to read and understand even if you know little or nothing about its specific context.
-   The code is easy to reuse and remix, in whole or in fragments.
-   The code is easy to write correctly, and errors are rapidly clear.

-------------------------------------------------------------------------------

## C Resources

-   Order a copy of the [C holy scripture](https://en.wikipedia.org/wiki/The_C_Programming_Language) by the prophets Kernighan and Ritchie.
-   Read and understand the [Ten Commandments of C Programming](http://www.lysator.liu.se/c/ten-commandments.html)
-   Read, bookmark, memorize [How to C in 2016](https://matt.sh/howto-c).
-   Read the [C FAQ](http://c-faq.com/)

-------------------------------------------------------------------------------

## General

-   Code shall be [C11](http://www.open-std.org/JTC1/SC22/WG14/www/docs/n1570.pdf) compliant.
-   Don't put an `else` right after a `return`. Delete the `else`, it's unnecessary and increases indentation level.
-   Don't leave debug `printfs` or dumps lying around.
-   Do not compare `x == true` or `x == false`. Use `(x)` or `(!x)` instead. `x == true`, in fact, is different from if `(x)!`.
-   Includes are split into three blocks and are sorted alphabetically in each block: 

    -   The main header
    -   Standard library includes
    -   Local includes

-   Include guards use `#pragma once`
-   All methods shall be thread safe.
-   Use [splint](http://www.splint.org/)

### Return Values


-   Any function must return one of the following values:
        -   logical value (zero or not zero)
        -   an error code (given as a negative number or zero) or a positive status value
        -   the count of read or written bytes/values for I/O functions
        -   the position or address (for search functions)
        -   a pointer
-   `NULL` indicates an error case, too.
-   Do NOT return structs or other larger types! These would get copied to the stack, resulting in expensive operations. Moreover, some compilers have trouble with larger return types. Use pointers to structs instead and take care of the structs lifetime.
-   If possible, prefer signed types over unsigned ones in order to be able to add error codes later on.


-------------------------------------------------------------------------------

## Naming and Formatting

-   Do NOT use CamelCase. Function, variable and file names as well as enums, structs or typedefs are written in the `lower_case_with_underscores` style.
-   Line length shall be no longer than 80 characters.

### Whitespace:

-   Use four spaces for indentation.
-   Tab characters shall not be used.
-   No whitespce at the end of a line.
-   Unix-style linebreaks `\n`, not windows-style `\r\n`
-   To handel linebreaks using git `core.autocrlf` see [this](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration).


### Control Structures

Use K&R bracing style: left brace at end of first line, cuddle else on both sides.
Note that function definitions are not control structures; left brace goes by
itself on the second line and without extra indentation. Break long conditions
after `&&` and `||` logical connectives.

For example:

    if (...) {
    } else if (...) {
    } else {
    }

    while (...) {
    }

    do {
    } while (...);

    for (...; ...; ...) {
    }

    switch (...) {
        case 1: {
            // When you need to declare a variable in a switch, put the block in braces
            int var;
            break;
        }
        case 2:
            ...
            break;
        default:
            break;
    }

Control keywords `if`, `for`, `while`, and `switch` are always followed by a 
space to distinguish them from function calls which have no trailing space. 

--------------------------------------------------------------------------------

## Documentation

-   Doxygen documentation is mandatory for all header files.
-   Every header file includes a general description about the provided functionality.
-   Every function must be documented - including parameters and return value.

--------------------------------------------------------------------------------

## Use `#pragma once`

`#pragma once` tells the compiler to only include your header once, there is
no need for three lines of header guards anymore.

Do this:

    #pragma once

Do NOT do this:

    #ifndef PROJECT_HEADERNAME
    #define PROJECT_HEADERNAME
    
    /**
     * Code
     */

    #endif /* PROJECT_HEADERNAME */

--------------------------------------------------------------------------------

## Use of the Preprocessor

Know what the [preprocessor is and does](https://en.wikipedia.org/wiki/C_preprocessor),
and what you shouldn't do even if you can.

The source code shall not define "magic numbers" (numeric constants); 
these SHALL be defined in the external or internal header file, as appropriate.

The preprocessor may be used for these purposes:

-   To create backwards compatibility with older code.
-   To improve portability by e.g., mapping non-portable system calls into more portable ones.
-   To create precise, small macros with high usability.

The preprocessor shall not be used for other work except when it significantly reduces
the complexity of the code. Macro names shall be uppercase when they represent constants,
and lowercase when they act as functions.

--------------------------------------------------------------------------------

## Use the stack

Often when you call a procedure you will want to have variables allocated in 
the caller's stack frame and pass pointers to them into the procedure you want
to call. This will be substantially faster than dynamically allocating memory 
with `malloc()` and much less error prone. Do this wherever appropriate.

> Memory leaks kill in space

C doesn't do garbage collection, so dynamically allocating data items is more
fiddly and you have to keep track of them to make sure they get freed.
Variables allocated on the stack are more 'idiomatic' where they are applicable.
Plus, you don't have to free them - this is a bonus for local variables.

Consider an architecture where your functions return a status or error code 
and pass data in and out using the stack.

--------------------------------------------------------------------------------

## C is not C++

C has no concept of module scope, so plan your use of includes, prototype 
declarations, and use of `extern` and `static` to make private scopes and
import identifiers.

### C Does not Support Exceptions

Becasue C [does not support exceptions](https://en.wikibooks.org/wiki/C_Programming/Error_handling)
Get to know what [setjmp()](https://en.wikipedia.org/wiki/Setjmp.h) 
and [longjmp()](https://en.wikipedia.org/wiki/Setjmp.h)
do. They can be quite useful for generic error handler mechanisms in lieu 
of structured exception handling functionality.


### C Has no Class

The equivalent of a constructor is an initialising function where you pass in a
pointer to the item you want set up. Often you can see this in the form of a
call to the function that looks like `setup_foo(&my_foo)`. It's better to
separate allocation from initialising, as you can use this function to
initialise an item you have allocated on the stack.
A similar principle applies to destructors.

--------------------------------------------------------------------------------
