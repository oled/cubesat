# Unresearched RTOSs

The following RTOSs have not been researched in depth as solutions for various
reasons including lack of time and seeming to have insufficient documentation
or active user community upon first glance.

-   [Atomthreads](http://atomthreads.com/)

    -   BSD Licence
    -   AVR/ATmega, STM8, ARM, MIPS
    -   Preemptive scheduler with 255 priority levels
    -   Round-robin at same priority level
    -   Semaphore
    -   Mutex
    -   Queue
    -   Timers
    -   Lightweight: the entire operating system fits into a few C files
    -   Highly portable ANSI C code not tied to any compiler
    -   Easy to read, well-documented, doxygen-commented code

-   [BeRTOS](http://www.bertos.org/)

    -   ARM, Cortex-M3, ARM ARM7TDMI, Atmel AVR, PowerPC (emu), x86 (emu), x86-64 (emu)
    -   Modified GNU GPLv2 licence
    -   preemptive multitasking kernel
    -   Signals, Semaphores, Messages
    -   Portable
    -   Hardware abstraction layer with large number of drivers

-   [cocoOS](http://www.cocoos.net/intro.html)

    -   Cooperative
    -   AVR, MSP430 and STM32
    -   coroutines
    -   Very portable
    -   BSD Licence

-   [Femto OS](http://www.femtoos.org/)

    -   GNU GPLv3
    -   Preemptive
    -   Very small footprint
    -   See page for ports (44 AVR devices)
    -   Written in C with port file

-   [FunkOS](http://funkos.sourceforge.net/)

    -   AVR, MSP430, Cortex-M3
    -   Modified Sleepycat License
    -   Fully preemptive multi-tasking environment
    -   Unlimited number of program "tasks" running from up to 255 different priority levels
    -   Tasks are independent programs, each with their own stack
    -   Round-robin with equal priority
    -   Lots of features
    -   FunkOS++ for C++ kernel

-   [nOS](https://github.com/jimtremblay/nOS)

    -   Mozilla licence
    -   Mostly C, some C++ and assembly
    -   AVR, MSP430, Cortex-M0-M3-M4, M16C, RX600, PIC24, Win32, POSIX, STM8
    -   Preemptive or cooperative scheduling (depending on your configuration)
    -   Can be tickless for battery-powered application
    -   Binary and counting semaphores
    -   Mutexes with priority ceiling or priority inheritance
    -   Queues for thread-safe communication
    -   Flags for waiting on multiple events
    -   Memory blocks for dynamic memory allocation
    -   Software timers with callback and priority
    -   Software interrupts (signal) with callback and priority
    -   Real-time module compatible with UNIX timestamp
    -   Software alarms with callback
    -   No limits on number of nOS objects, except your available memory
    -   Tiny footprint (as low as 1KB ROM/few bytes of RAM)
    -   Fully configurable (ROM and RAM)
    -   Open source, royalty free
    -   Win32 and Linux simulator available
