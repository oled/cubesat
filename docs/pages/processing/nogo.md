# Options Ruled Out

## Unfortunately optimal solution is not possible

Porting an operating system to achieve the ideal solution is not possible with
current resources (see options ruled out below). The system must be divided
between the processors using an individual bare metal program on each. The
problem of resource sharing should now be avoided unless it is absolutely
required. Also when handling data sharing between the two different
architectures the following must be kept in mind, depending on
architecture differences and method:


-   Endianness

-   Standard APIs 

    -   Implementations may differ
    -   Could have adverse effects on shared data
    -   For example `memcpy()` on ARM is implemented in a unique way internally

-   Types and structures

    -   Strictly use alignment by the tool chains
    -   Alignment and padding for each architecture might be totally different

-   Synchronisation

    -   Will need doorbell interrupts so each CPU can interrupt the other to notify each other of communication.
    -   Alternative is polling, although has low throughput.

-   Atomicity

    -   Multi-word structs to shared memory is problematic
    -   No synchronisation mechanisms you might use in other circumstances

        -   No bus-locking
        -   No disabling interrupts

    -   Could use spin-locks to control read-write access, but they are wasteful   

-   Concurrent memory access

    -   Concurrent writes to memory on the same address may be unpredictable (multi-port SRAM)
    -   If so it can not be done
    -   Cache 

-   Memory/instruction order barriers

    -   Memory barriers must be implemented to ensure that writes are observed on the memory bus in the expected order
    -   Many Cortex ARM CPUs have out-of-order execution and store behavior
    -   The address range of the memory must be uncachable or flushed

The easiest way to do communications between two processors is to use a
[ring buffer](https://en.wikipedia.org/wiki/Circular_buffer) such that the
writing head and tail pointers are the natural word size of the memory
and are maintained by the writing processor and reading processor respectively.
The basic methods of interprocess communication include,

-   Semaphores & Mutex
-   Event Flags
-   Shared Memory

    -   Cache coherency Required
    -   Cache coherent hardware networks are possible (CCI-400)
    -   Process Synchronization Required

-   Message Passing

    -   No shared memory
    -   `send(pid, message)`
    -   `receive(pid, message)`
    -   Blocking send
    -   Nonblocking send
    -   Blocking receive
    -   Nonblocking receive
    -   Buffering, a framework to buffer the messages

-   Pipes

    -   Link one processes output as input to another
    -   One-way communication
    -   Can be blocking or non-blocking

-   Signals

    -   Event notification
    -   When a notification is received the system may interrupt its normal flow

It is critical that when processes communicate they are both synchronized
and cache coherency is maintained.
ARM has the wait for event `WFE` and send event `SEV` instructions to keep
synchronization between cores.
Modern day interprocessor interconnect protocols are somewhat similar to
the internet protocols TCP/UDP, maybe a little simpler. Processors can
shout about modifying a value, drop packets, etc. Some of the
interconnect protocols are AMBA by ARM, IEEE standard "scalable coherent
interface".

## [FreeRTOS](http://www.freertos.org/)

Operating system chosen for its features, community, documentation, and establishment
as a reliable OS over 12 years.

-   General Information

    -   Modified GNU GPL
    -   co-operative, pre-emptive, and hybrid modes
    -   Small footprint
    -   Portable
    -   ARM, AVR, AVR32, ColdFire, HCS12, IA-32, Cortex-M3-M4-M7, MicroBlaze, MSP430, PIC, PIC32, Renesas H8/S, RX100-200-600-700, 8052, STM32, EFM32
    -   Large active community
    -   Tickless mode for low power applications
    -   Safety critical version available

-   Mission Specific

    -   Mature OS, 12 years in development.
    -   Porting FreeRTOS to a completely different and as yet unsupported microcontroller is not a trivial task.
    
        -   [FreeRTOS porting documentation](http://www.freertos.org/FreeRTOS-porting-guide.html)
        -   [Atmel SAM4S demo](http://www.freertos.org/Atmel_SAM4_SAM4S-EK_RTOS_Demo.html) is provided.

    -   Data Storage

        -   Does not natively support any filesystems
        -   Third party experimental support for FAT filesystem available with [FreeRTOS+FAT](http://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_FAT/index.html)
        -   Third party support for FAT SL filesystem available with [FreeRTOS+FAT SL](http://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_FAT_SL/FreeRTOS_Plus_FAT_SL.shtml)
        -   Third party solution for a journaling filesystem with [Reliance Edge](http://www.freertos.org/FreeRTOS-Plus/Fail_Safe_File_System/Reliance_Edge_Fail_Safe_File_System.shtml)

    -   Data Transmission

        -   Does not natively support any network protocols
        -   Third party experimental support for TCP with [FreeRTOS+TCP](http://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/index.html)
        -   Third party support for UDP with [FreeRTOS+UDP](http://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_UDP/FreeRTOS_Plus_UDP.shtml)

    -   Provides a easy way to map user commands with CLI type interface using [FreeRTOS+CLI](http://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_CLI/FreeRTOS_Plus_Command_Line_Interface.shtml)

    -   Provides third party solution to security, encryption, and authentication using [WolfSSL](http://www.freertos.org/FreeRTOS-Plus/WolfSSL/WolfSSL.shtml).


### Porting FreeRTOS to SMP:

FreeRTOS is divided into two layers, the hardware independent layer and which
defines most of the operating system and the portable layer which is
responsible for hardware-specific processing (such as context switching).
The required hardware independent files `list.c` and `tasks.c` provide 
the minimum high-level functionality for task scheduling.
Note that a FreeRTOS "task" is the equivalent to a thread in
standard operating systems.

-   Hardware Independent

    -   `coroutine.c`: limited memory efficient tasks
    -   `list.c`: data structure for the scheduler
    -   `queue.c`: priority queue for application message passing
    -   `tasks.c`: task management functionality
    -   `timers.c`: provides software timers

-   Portable

    -   `port.c`: exposes the portable API from the hardware independent layer
    -   `heap.c`: port-specific memory allocation and deallocation functions

A `main` function must also be provided that is responsible for
creating all initial application tasks and starting the FreeRTOS
scheduler.


-   Hardware must be defined

    -   To support for pre-emptive scheduling timer and interrupt peripherals are provided for each core
    -   All devices must be defined as system "drivers"
    -   Buses must be defined
    -   Special-purpose read-only registers had to be configured to allow processors to identify themselves to software

-   Core Ignition
-   Memory Layout
-   Task Control Block
-   Synchronisation   
-   Scheduler

### Conclusion

Despite a [port of FreeRTOS for the Atmel SAM4S](http://www.freertos.org/Atmel_SAM4_SAM4S-EK_RTOS_Demo.html),
building on top of it to support symmetric multiprocessing on a custom
board would require significant modifications and consideration would
have to be given at every level of design to the systems hardware
and software requirements. Supporting asymmetric multiprocessing would
require even more modification to the scheduler due to the need to
copy task stacks between private memory areas when moving a task from
one core to another.

It is not possible to port FreeRTOS with the current software team size
and other development that must also be done. There will be similar
requirements for any OS which does not support SMP natively and therefore
using a symmetric multiprocessing operating system to achieve the ideal
solution must be abandoned for a much less ideal approach.



## [RIOT](http://www.riot-os.org/)

The RIOT operating system has been eliminated from being a possible candidate
for use in the project due to a current lack of filesystem support.
Besides the lack of filesystem support it would have been a good choice and
supports both Asymmetric Multiprocessing and Symmetric Multiprocessing.

-   General Information

    -   GNU LGPL
    -   Standard C/C++ programming
    -   Standard toolchain- gcc, gdb, valgrind
    -   MSP430, ARM7, Cortex-M0, Cortex-M3, Cortex-M4, x86
    -   Has official docker containers for CI
    -   Zero learning curve for embedded programming
    -   Code once for 8-bit, 16-bit, and 32-bit
    -   Partial POSIX compliance
    -   Develop on Linux using the _native port_, deoploy on embedded device
    -   Microkernel architecture
    -   Tickless scheduler for low power environments
    -   Real-time capability (ultra-low interrupt latency)
    -   Low multi-threading overhead (<25 bytes per thread)
    -   Min RAM: 1.5kB
    -   Min ROM: 5kB

-   Specific

    -   Does not support any filesystems natively or otherwise

## Protothreads

Protothreads are extremely lightweight stackless threads designed for severely
memory constrained systems, such as small embedded systems or wireless sensor
network nodes. Protothreads provide linear code execution for event-driven
systems implemented in C. Protothreads can be used with or without an
underlying operating system to provide blocking event-handlers. Protothreads
provide sequential flow of control without complex state machines or full
multi-threading. 

-   Very small RAM overhead - only two bytes per protothread and no extra stacks
-   Highly portable - the protothreads library is 100% pure C and no architecture specific assembly code  
-   Can be used with or without an OS 
-   Provides blocking wait without full multi-threading or stack-switching 
-   Freely available under a BSD-like open source license 
-   Event-driven protocol stacks 

[Full documentation](http://dunkels.com/adam/pt/)

This option has been eliminated due to the lack of process preemption,
a requirement for system safety.

## Time-Triggered Scheduling

Libraries found are not portable, safe, mature, or reliable. Would likely 
have to implement ourselves or build on something partially finished.
This option has been ruled out because while implementing this is
possible it would require the attention of the entire existing code
team to implement a robust enough implementation to trust for spacefaring
within the time constraints.

-   Predictable
-   Deterministic
-   Tasks with precise timing
-   Use timer interrupts to execute tasks with microsecond precision
-   Tasks have a timer and offset
-   Used for a long time in safety-critical systems due to the inherently deterministic behaviour

[Example Time-Triggered Cooperative scheduler](https://github.com/chrispbarlow/arduino-tasks)
