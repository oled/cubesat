# Processing

The [Atmel GNU Toolchains](http://www.atmel.com/webdoc/atmelstudio/atmelstudio.GNU_Toolchain.html)
have details for their use with both the ARM and AVR architectures
that will be used in the project. The 8 and 32-bit specific GCC
command line options for optimization are also of note.
The [Atmel Software Framework (ASF)](http://asf.atmel.com/docs/latest/)
provides many utilities for working with the Atmel SAMS4 core processors.
There is also a [detailed overview provided by the Atmel director of 
microcontrolers](http://www.electronicproducts.com/Software/Development_Tools_and_Software/Atmel_AVR_Software_Framework_lets_you_stand_on_the_shoulders_of_software_giants.aspx).

-   Advantages of process cooperation

    -   Information Sharing
    -   Computation Speed Up
    -   Modularity
    -   Convenience

-   Disadvantages of process cooperation

    -   Data corruption
    -   Deadlocks
    -   Increased Complexity
    -   Synchronization Required
    -   Cache Coherency Required


A solutions to the multiprocessing and process preemption requirements are
needed. The ideal, robust, and elegant solution to this is to use an embedded 
operating system that supports process preemption and preferably with a
hybrid approach with cooperative processing.
The two processing methods that meet the requirements of the project are
[asymmetric multiprocessing](https://en.wikipedia.org/wiki/Asymmetric_multiprocessing) (AMP)
and [symmetric multiprocessing](https://en.wikipedia.org/wiki/Symmetric_multiprocessing) (SMP).
Usually SMP is used to gain more processing power to manage a workload and AMP
is used when different CPU architectures are optimal for specific activities.

-   AMP

    -   Each core has its own kernel
    -   Each kernel may be different or identical
    -   Each processor has its own OS image in memory
    -   Can have shared memory for interprocessor communications
    -   Processors are unaware of each other unless told to communicate
    -   Resource sharing must be managed manually which is much more dangerous and complicates design

-   SMP

    -   One kernel run by all cores
    -   Single OS runs all processors
    -   Single OS image in memory
    -   The OS provides parallelism
    -   The OS divides tasks between processors
    -   The OS manages resource sharing


The ideal solution for this projects specific system would be symmetric
multiprocessing across  the three homogeneous cores (ARM) and to have that
cluster perform asymmetric multiprocessing with the other heterogeneous
(AVR) processor. This is possible because logically the processors combined in an
SMP configuration look like a single core and can be included in the AMP system.
This solution would look like the following:

&nbsp;
![Hybrid Multiprocessing](docs/dia/processing.png)
&nbsp;

In this model the core processors share some memory space, and the OS is a single
instance that runs on all the CPUs, and divides the work between them. Some kind
of communication facility must be provided to the CPUs and this is normally done
with shared memory. The communications between the individual CPUs in SMP is
transparent and completely managed by the OS. Then the single SMP system that
is formed is linked with the single ranging processor, which has its own 
memory for its own OS, in an AMP configuration.

A much less ideal solution would be to use asymmetric multiprocessing across all
processors. In this design shown below each CPU has its own data storage with
its own instance of an OS. Each CPU's communications with the others must be
manually defined possibly through some centralized shared memory store. This
is a much more primitive approach and will severely impact code complexity
which may negatively affect the reliability of the program.

&nbsp;
![Asymmetric Multiprocessing](docs/dia/AMP_processing.png)
&nbsp;

Of course a final option is to forgo the OS which will greatly reduce the
reliability of the system. Without filesystem support there would be no way
to protect data from corruption resulting from a power loss during write,
and there would be no way to track multiple files so all data would need to
be stored in a single [flat file](docs/pages/data.md). Without an OS only
crude communications between processors would be possible, and even safe
for that matter, and each processor would need to be a single purpose
processor, or use a state machine to switch between tasks.

More detailed information can be found in this
[QNX Software Systems document](https://cache.freescale.com/files/32bit/doc/brochure/PWRARBYNDBITSRAS.pdf)
on running AMP, SMP and BMP on embedded systems.

In order to implement hardware based mutual exclusion primitives the processors
must have built-in atomic instructions that work across all processors such as
the x86 `xchg` instruction that exchanges a rester value with one at a specified
memory location. Without hardware support implementation of both 
[mutex](https://en.wikipedia.org/wiki/Mutual_exclusion) and 
[semophore](https://en.wikipedia.org/wiki/Semaphore_%28programming%29) control
systems must be implemented in software, this would also require hardware
support for memory barriers, on x86 the `mfence` instruction performs this
function.

## None of these are possible

Unfortunately [none of these solutions are possible](docs/pages/processing/nogo.md)
, and although there are
[some unresearched OSs](docs/pages/processing/unresearched.md)
they all carry the same level of complexity that can not be introduced to the
project.

## Compromise

A simple message passing system with isolated single purpose programs like a
form of very primitive Asymmetric Multiprocessing.

![Message Passing](docs/dia/message_dia.png)
