%% This  section  of  the  SRS  should  contain  all  of  the  software
%% requirements  to  a  level  of  detail  sufficient  to enable designers
%% to design a system to satisfy those requirements, and testers to test
%% that the system satisfies those requirements. Throughout this section,
%% every stated requirement should be externally perceivable by users,
%% operators, or other external systems. These requirements should include
%% at a minimum a description of every input (stimulus) into the system,
%% every output (response) from the system, and all functions performed
%% by the system in response to an input or in support of an output.
\documentclass[../main.tex]{subfiles}
\begin{document}
\part{Specific Requirements}
\onehalfspacing


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Interfaces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This  should  be  a  detailed  description  of  all  inputs  into  and  outputs
%% from  the  software  system.  It  should complement the interface descriptions
%% in the previous PART II and should not repeat information there.
%%
%% It should include both content and format as follows:
%%
%%     - Name of item;
%%     - Description of purpose;
%%     - Source of input or destination of output;
%%     - Valid range, accuracy, and/or tolerance;
%%     - Units of measure;
%%     - Timing;
%%     - Relationships to other inputs/outputs;
%%     - Screen formats/organization;
%%     - Window formats/organization;
%%     - Data formats;
%%     - Command formats;
%%     - End messages.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{External Interface Requirements}
This section describes in detail all the inputs into and outputs from
the software system. Note that \emph{all} measurements shall be metric.
Also of note is that the 9DOF sensor is a combined gyroscope, 
accelerometer and magnetometer.

\subsection{Hardware Interfaces}

\subsubsection{Core Processors}
\begin{itemize}
    \item Name: Atmel-11100K-ATARM-SAM4S (ATSAM4SD16CA)
    \item Mnemonic: SAM4S
    \item Purpose: The core processors shall perform the primary logic
        required for the satellite. They shall coordinate and prioritize
        all other operations and running processes.
    \item \href{https://drive.google.com/open?id=0B8fB1JMORo9pMFVVSzRmOTVYb0IyclFwRWoxRWZuN0tKVzZ3}
          {Datasheet Link}
    \item Relevant Properties:
    \begin{itemize}
        \item 32-bit
        \item ARM Cortex-M4 RISC
        \item Single core
        \item No FPU
        \item 12 cycle interrupt latency
        \item 2 Kbytes cache
        \item Low power modes
        \item Temperature sensor
        \item Can perform PWM
    \end{itemize}
    \item Source of inputs:
    \begin{itemize}
        \item The secondary memory or data store
        \item Camera processor
        \item Temperature sensor
        \item 9DOF
        \item Ranging Processor
        \item GPS
    \end{itemize}
    \item Destination of outputs:
    \begin{itemize}
        \item The secondary memory or data store
        \item Ranging Processor
        \item Iridium Module
        \item RFM69HW intersat radio communication module
        \item Lithium-1 UHF ground communication module
        \item Antenna deployer
        \item Balloon heater
        \item Balloon doors
        \item Panel deployer
    \end{itemize}
    \item Relationship to other inputs/outputs:
        The primary logic is implemented on the core processors. All other
        systems are coordinated by processing the inputs to the core
        processors and producing the correct outputs.
\end{itemize}

\subsubsection{Ranging Processor}
\begin{itemize}
    \item Name: ATtiny411
    \item Purpose: Perform ranging calculations
    \item \href{https://drive.google.com/drive/folders/0B8fB1JMORo9pdmsyVGZyVE9vRUk}
          {Datasheet Link}
    \item Relevant Properties:
    \begin{itemize}
        \item 8-bit
        \item AVR (RISC)
        \item 32 8-bit registers
        \item Low power modes
        \item Can perform PWM
        \item internal/external interrupts
    \end{itemize}
    \item Source of inputs:
    \begin{itemize}
        \item Core Processors
        \item RFM69HW intersat radio communication module
        \item GPS
    \end{itemize}
    \item Destination of outputs:
    \begin{itemize}
        \item Core Processors
    \end{itemize}
\end{itemize}

\subsubsection{Camera}
\begin{itemize}
    \item Name: TBD
    \item Purpose: Verification of drag brake deployment
    \item Destination of outputs:
    \begin{itemize}
        \item Camera Processor
    \end{itemize}
    \item Data Format: serial
\end{itemize}

\subsubsection{Camera Processor}
\begin{itemize}
    \item Name: Raspberry Pi Zero
    \item Purpose: Operate camera to verify drag brake deployment and compress
        the photogrpah to prepare it for transmission to ground control.
    \item \href{https://drive.google.com/open?id=0B8fB1JMORo9pZnM4akJkUDRPRFFRNExyVmVGNjhtWHNvazBn}
          {Datasheet Link}
    \item Source of inputs:
    \begin{itemize}
        \item Camera
    \end{itemize}
    \item Destination of outputs:
    \begin{itemize}
        \item Core Processors
    \end{itemize}
\end{itemize}

\subsubsection{3-Axis Gyroscope}
\begin{itemize}
    \item Name: 9DOF
    \item Purpose: Attitude Determination and Control, track satellite orientation.
    \item \href{https://drive.google.com/open?id=0Bw9VsZ62sYFsQ2tPV1JMb09Ga0U}
          {Datasheet Link}
    \item Destination of outputs:
    \begin{itemize}
        \item Core Processors
    \end{itemize}
\end{itemize}

\subsubsection{Accelerometer}
\begin{itemize}
    \item Name: 9DOF
    \item Purpose: Attitude Determination and Control.
    \item Destination of outputs:
    \begin{itemize}
        \item Core Processor
    \end{itemize}
\end{itemize}

\subsubsection{Magnetometer}
\begin{itemize}
    \item Name: 9DOF
    \item Purpose: Attitude Determination and Control, measure the force of the earth's magnetic field.
    \item Destination of outputs:
    \begin{itemize}
        \item Core Processor
    \end{itemize}
\end{itemize}

\subsubsection{Coarse Sun Sensors}
\begin{itemize}
    \item Name: NanoPower P110 (integrated into solar panels)
    \item Purpose: Attitude Determination and Control, track location of the sun.
    \item \href{http://gomspace.com/documents/ds/gs-ds-nanopower-p110-2.4.pdf}
          {Datasheet Link}
    \item Destination of outputs:
    \begin{itemize}
        \item Core Processors
    \end{itemize}
\end{itemize}

\subsubsection{Magnetorquer}
\begin{itemize}
    \item Name: NanoPower P110 (integrated with solar panels)
    \item Purpose: Attitude Determination and Control, change satellite orientation.
    \item \href{http://gomspace.com/documents/ds/gs-ds-nanopower-p110-2.4.pdf}
          {Datasheet Link}
    \item Source of inputs:
    \begin{itemize}
        \item Core Processor
    \end{itemize}
\end{itemize}

\subsubsection{GPS}
\begin{itemize}
    \item Name: PiNav
    \item Purpose: Attitude Determination and Control, and drag determination.
        GPS will be used to determine the orbital drift from the other two
        satellites.
    \item \href{}
          {Datasheet Link}
    \item Source of inputs:
    \begin{itemize}
        \item Patch antenna
    \end{itemize}
    \item Destination of outputs:
    \begin{itemize}
        \item Core processors
        \item Ranging processor
    \end{itemize}
\end{itemize}

\subsubsection{Storage}
TBD

\subsubsection{Deployables}
\begin{itemize}
    \item Name: Various
    \item Purpose: Various items that need to be deployed after the satellite
        has been launched due to size constraints.
    \item Source of inputs:
    \begin{itemize}
        \item Core Processors
    \end{itemize}
\end{itemize}

\subsection{Communication Interfaces}
This should specify the various interfaces to communications such as
network protocols, etc.

\subsubsection{Lithium-1 Radio}
\begin{itemize}
    \item Name: Lithium-1
    \item Purpose: The Lithium radio is our primary communication method. The system 
        operates over UHF in the amateur band using the AX.25 protocol. Operating
        in the amateur band requires all data to be unencrypted with the exception of
        command and control. Command and control packets may use any means necessary
        to ensure that no unauthorized commands will be processed by the satellite.
        The Lithium-1 radio is half-duplex meaning that it is only capable of either
        transmitting or receiving at any given time, but not both. It will also
        share a multiplexed connection to a single UHF antenna that is also used by
        the RFM69HW radio and it will not be possible to operate during times the
        RFM69HW is using the antenna.

        AX.25 is a data-link layer protocol in the second layer of the OSI-model.
        This part of the network stack is mainly responsible for establishing
        connections and transferring data encapsulated in frames between nodes
        and detecting errors introduced by the communications channel. It is
        frequently used with some other network layer such as IPv4 with TCP
        or UDP.
    \item \href{}
          {Datasheet Link}
    \item Source of inputs:
    \begin{itemize}
        \item Core Processors
        \item Ground control
        \item Malicious attackers
    \end{itemize}
    \item Destination of outputs:
    \begin{itemize}
        \item Ground control
    \end{itemize}
\end{itemize}


\subsubsection{RFM69HW}
\begin{itemize}
    \item Name: RFM69HW
    \item Purpose: Fulfil the secondary objective of inter-satellite
        communications using a point to point radio network with other 
        cubesats for ranging experiments. Because it operates in the
        same frequency band as the Lithium-1 and shares the antenna
        network it is capable of acting as a backup. Sharing a
        multiplexed connection to the UHF antenna with the Lithium-1
        also means that it will not be possible to operate simultaniously
        with the Lithium-1
    \item \href{}
          {Datasheet Link}
    \item Source of inputs:
    \begin{itemize}
        \item Core Processors over SPI
        \item Other satellites in the constellation
        \item Ground control
        \item Malicious attackers
        \item Malicious satellites
    \end{itemize}
    \item Destination of outputs:
    \begin{itemize}
        \item Core Processors
    \end{itemize}
    \item Relationship to other inputs/outputs:
        Ranging experiments will be performed using the
        highly accurate pulse per second signal from the GPS.
        This is accomplished by communicating the arrival of
        a pulse with the other satellites and then measuring
        the arrival time against the local pulse per second
        signal.
\end{itemize}

\subsubsection{Iridium}
\begin{itemize}
    \item Name: Iridium modem model 9603
    \item Purpose: The Iridium satellite constellation allows for small continuous
        data transfers. This will become more important later in the satellites
        lifespan allowing for closer to real time data samples without having
        to wait for overhead communications, and also more accurately determining
        where re-entry happens. It is also possibly a third backup for other
        communications.
    \item \href{https://satphoneshop.com/downloads/9602DevelopersGuide.pdf}
          {Iridium 9602 SBD Transceiver Developer's Guide}
    \item Source of inputs:
    \begin{itemize}
        \item Ground control through the iridium satellite constellation
    \end{itemize}
    \item Destination of outputs:
    \begin{itemize}
        \item Ground control
        \item Anyone listening
    \end{itemize}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Functional Requirements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Functional  requirements  should  define  the  fundamental  actions  that
%% must  take  place  in  the  software  in accepting and processing the inputs
%% and in processing and generating the outputs. These are generally listed as
%% "shall" statements starting with "The system shall..."
%%
%% These include
%%     - Validity checks on the inputs
%%     - Exact sequence of operations
%%     - Responses to abnormal situations, including
%%         * Overflow
%%         * Communication facilities
%%         * Error handling and recovery
%%     - Effect of parameters
%%     - Relationship of outputs to inputs, including
%%         * Input/output sequences
%%         * Formulas for input to output conversion
%%
%% It may be appropriate to partition the functional requirements into
%% subfunctions or subprocesses. This does not imply that the software design
%% will also be partitioned that way.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{System Features}
% Notes:
%   - ADCS thinks ahead/remembers about orientation leaving dark side
%     so that it can pre-position for sunlight coming out.
%   - Magnetorquers shall not run during magnetometer reads


%%%%%%%%%%%%%%%%%%%%
% Post-Launch Mode %
%%%%%%%%%%%%%%%%%%%%
\subsection{Post-Launch Mode}

\subsubsection{Compliance with NanoRacks regulations}
Following CubeSat launch only a timer shall run for thirty minutes before
initializing the standard operating mode. Absolutely no transmissions nor
deployments shall be done during this time.

\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        This is a requirement that must be followed to legally operate as per the
        % Share drive link
        \href{http://nanoracks.com/wp-content/uploads/Current_edition_of_Interface_Document_for_NanoRacks_Smallsat_Customers.pdf}
             {NanoRacks CubeSat Deployer (NRCSD) Interface Control Document}.

    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Release of power inhibiting switches.
            \item Response: Initiating a thirty minute delay timer to satellite operations.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
        \begin{enumerate}
            \item The system shall not transmit any signals.
            \item The system shall not deploy any appendages.
            \item The system shall not run any processes besides a timer.
            \item The system shall reset the timer every time the remove before flight flag is replaced.
            \item The system shall reset the timer every time the deploy switches are returned to the open state.
            \item The system shall initiate the standard operating mode no sooner than 30 minutes after launch.
            \item The system shall save its timer state in non-volatile memory.
        \end{enumerate}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Standard Operating Mode %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Standard Operating Mode}

\subsubsection{Deployables}
\subsubsection{Drag Brake}
\begin{itemize}
	\item \textbf{Introduction and Purpose of Feature:}
	The system must be able to initiate deployment of the drag brake system.
	\item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Deploy user command received.
            \item Stimulus: Deploy timer trigger.
            \item Response: The system executes the command with normal priority.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    	\begin{enumerate}
    		\item The system shall automatically deploy the drag brake after a timer expires.
    		\item The system shall not deploy the drag brake under emergency conditions.
    		\item The system shall open balloon doors on receipt of command from core processors.
    		\item The system shall activate balloon heater on receipt of command from core processors.
    		\item The system shall provide feedback codes (i.e. success or fail) for deployment status to core processors for relay to ground station.
    		\item The system shall communicate with core processor requesting activation of the camera processor for verification of drag brake deployment.
		\end{enumerate}
\end{itemize}
\subsubsection{Antennas}
\begin{itemize}
	\item \textbf{Introduction and Purpose of Feature:}
	The system must be able to initiate deployment of the UHF antenna.
	\item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Deploy command received.
            \item Response: The system executes the command with normal priority.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    	\begin{enumerate}
    		\item The system shall receive command to deploy antenna from core processors.
    		\item The system shall activate deployment of antenna.
    		\item The system shall provide feedback codes (i.e. success or fail) for deployment status to core processors for relay to ground station.
    	\end{enumerate}
\end{itemize}

\subsubsection{Data Collection}
\begin{itemize}
	\item \textbf{Introduction and Purpose of Feature:}
		The Data Collection system must provide sufficient storage for all sensor data. When in range for transmitting to ground station, the data collection system must allow access for transmission of sensor data.
	\item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Data store access requested.
            \item Response: The system executes the command with normal priority.
        \end{itemize}
     \item \textbf{Associated Functional Requirements:}
    	\begin{enumerate}
            \item The system shall begin data collection $1.5$ hours after launch.
    		\item The system shall store raw data from the following sensors:  
    		\begin{itemize}
    			\item Accelerometer
    			\item GPS
    			\item Magnetometer
    			\item Sun sensor
    			\item Temperature Sensor
    		\end{itemize}
            \item The system shall use the highly accurate Pulse Per Second (PPS) signal from the GPS to calculate and store ranging data.
    	\end{enumerate}
\end{itemize}

\subsubsection{ADCS}
\begin{itemize}
	\item \textbf{Introduction and Purpose of Feature:}
		The ADCS system must be able to receive input from the 9DOF module, sun sensors, and communicate both ways with the core processors in order to maintain an orientation that maximizes power acquisition.
	\item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Input of normal range data from gyroscope and sun sensors.
            \item Response: Attitude correction.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    	\begin{enumerate}
    		\item The system shall receive satellite orientation data from the 9DOF module.
    		\item The system shall receive intensity data from the sun sensors.
    		\item The system shall compute satellite orientation for optimum power acquisition.
    		\item The system shall direct magnetorquers using PWM to change satellite orientation.
    		\item The system shall communicate to and from core processors.
    	\end{enumerate}
\end{itemize}

\subsubsection{Camera Operation}
\begin{itemize}
	\item \textbf{Introduction and Purpose of Feature:}
		The camera system shall remain powered off until needed to verify successful deployment of the drag brake.		
	\item \textbf{Stimulus/Response Sequence:}
		\begin{itemize}
			\item Stimulus: Drag brake deployed status received from core processors.
			\item Stimulus: Take picture user command received.
	        \item Response: The system wakes the camera processor, which takes a photograph of the drag brake, compresses it, stores it on board, and transfers it to be sent to ground control.
		\end{itemize}
    \item \textbf{Associated Functional Requirements:}
    	\begin{enumerate}
    		\item The core processors shall wake the camera processor.
    		\item The camera processor shall command power on sequence for camera.
    		\item The camera processor shall capture the photograph.
    		\item The camera processor shall compress photograph for storage/transmission.
    		\item The camera processor shall store photos in its own data store.
    		\item The camera processor shall power off upon successful capture and storage of photograph.
            \item The camera processor shall wake and provide image data to the core processors for transmission upon request.
    		\item The camera processor shall power on and repeat the photograph sequence upon user command.
    	\end{enumerate}
\end{itemize}


%%%%%%%%%%%%%%%%%%
% Low Power Mode %
%%%%%%%%%%%%%%%%%%
\subsection{Low Power Mode}

\subsubsection{Return to normal operation mode}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        The system must be able to return to normal operation mode once the system 
        has restored its power level to sustain normal operation.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Power level falls below a safe level.
            \item Response: The system shuts down all non-critical processes.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall resume all halted non-critical processes.
    \end{enumerate}
\end{itemize}

\subsubsection{Critical Processes continue operation}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        The system shall maintain the ability to execute all system
        critical processes.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: The system enters low power mode.
            \item Response: The system continues running all system critical processes.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall receive and process commands.
        \item The EPS shall remain in operation.
        \item The EPS heater shall remain in operation.
    \end{enumerate}
\end{itemize}


\subsubsection{Non-critical systems shutdown}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        The system will shut down all systems that are not
        critical to satellite health, or for receiving and
        processing user commands.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: The system enters low power mode.
            \item Response: The system shuts down all non-critical systems.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall halt all transmissions, unless responding to a user
            command.
        \item All non-critical systems shall shut down.
        \item All non-critical processes shall be halted.
    \end{enumerate}
\end{itemize}


\subsubsection{Return to normal operation mode}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        The system must be able to return to normal operation mode once the system 
        has restored its power level to sustain normal operation.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Power level returns to a safe level.
            \item Response: The system executes the command with normal priority.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall resume all non-critical systems as needed by
            the normal operation mode.
        \item The system shall resume all halted non-critical processes.
    \end{enumerate}
\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User Command Execution Mode %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Assuming a RTOS is used which is very likely the system shell can serve
% as the interface for command execution.
%
\subsection{User Command Execution Mode}

\subsubsection{Standard Commands}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        The system must be able to receive commands from ground control for standard
        operation purposes. Standard commands are required to run processes that must
        not interrupt other critical processes.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Standard system command received.
            \item Response: The system executes the command with normal priority.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall authenticate all commands.
        \item The system shall be able to receive a standard command over the Lithium-1 interface.
        \item The system shall be able to receive a standard command over the RFM69HW interface.
        \item The system shall be able to receive a standard command over the Iridium network.
        \item The system shall return an error code on failure.
        \item The system shall execute the command with normal priority.
        \item The system shall have the ability to execute any OS shell command.
        \item The system shall have the ability to receive a sample data download command.
        \item The system shall have the ability to receive a image data download command.
        \item The system shall be able to receive a command to delete software backups.
        \item The system shall be able to receive a command to restore software backups.
        \item The system shall be able to receive a command to downsample data.
        \item The system shall be able to receive a command to delete data.
        \item The system shall be able to receive a command to transmit system
            health data.
        \item The system shall be able to receive a command to take a photograph of the drag brake.
    \end{enumerate}
\end{itemize}

\subsubsection{Forceful Commands}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        The system must be able to receive commands for emergency operations. These commands
        are for executing commands over potentially misbehaving emergency processes.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Forceful command received.
            \item Response: Command execution begins during the soonest possible CPU cycle.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall be able to receive a forceful command over the RFM69HW interface.
        \item The system shall be able to receive a forceful command over the Iridium network.
        \item The system shall execute the command with the highest priority.
        \item The system shall return an error code on failure.
        \item Any standard command shall be executable as a forceful command.
        \item The system shall accept a command to raise priority of data
            collection and transmission processes for end of life data 
            collection during the final orbital decay when the system
            will likely be in emergency mode.
    \end{enumerate}
\end{itemize}


%%%%%%%%%%%%%%%%%%
% Emergency Mode %
%%%%%%%%%%%%%%%%%%
\subsection{Emergency Mode}

This mode will be used to correct critical satellite health conditions
detected by satellite health telemetry. It shall run at a higher priority
until satellite health has been restored at which time normal operation
shall resume.

\subsubsection{Solar Cell Failure Detection}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        The system must be able to detect and adapt to solar cell
        failure. The system will need to complete the mission using
        a reduced power budget in the case of a solar cell failure.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Solar cell failure detected.
            \item Response: The system shall reduce its operations to conserver power.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall attempt triage.
        \item The system shall notify ground control at the next possible opportunity.
        \item The system shall cut non-essential processes.
        \item The system shall reduce data sample rate.
        \item The system shall reduce measurement rate.
    \end{enumerate}
\end{itemize}

\subsubsection{ADCS initial stabilization tumble recovery}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        If the system upon waking from post-launch sleep
        mode detects a tumble condition it must immediately
        correct the situation.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Tumble condition detected.
            \item Response: The system shall suspend all processes until the tumble condition is corrected.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The ADCS tumble recovery process shall run at high priority.
        \item The system shall shall use the 9DOF and Magnetorquers to determine the correct corrective action.
        \item The system shall take a quorum with all available processors to make a final decision on corrective action.
        \item In the case of a two processor quorum there shall be a finite number of attempts to agree before an action is taken to avoid deadlock.
        \item In a tumble situation the system shall dedicate all available power
            correcting the situation using the magnetorquers.
    \end{enumerate}
\end{itemize}

\subsubsection{ADCS emergency recovery}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        At any given time if a significant level of drift in
        satellite orientation is detected the system must be
        able to dedicate all available resources to correcting
        the orientation. Such a situation could potentially
        threaten the health of the satellite by causing
        appendages to become tangled in the lines to the
        drag brake.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Out of safety limit satellite orientation detected.
            \item Response: The system shall suspend all processes until the tumble condition is corrected.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The ADCS tumble recovery process shall run at high priority.
        \item The system shall shall use the 9DOF and Magnetorquers to determine the correct corrective action.
        \item The system shall take a quorum with all available processors to make a final decision on corrective action.
        \item In the case of a two processor quorum there shall be a finite number of attempts to agree before an action is taken to avoid deadlock.
    \end{enumerate}
\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%
% System Update Mode %
%%%%%%%%%%%%%%%%%%%%%%
\subsection{System Update Mode}

\subsubsection{System Update Command}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        The system should be able to update its software if needed.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Software update command and data received.
            \item Response: The system updates the software.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall authenticate the update command.
        \item The system shall decrypt and authenticate the data.
        \item The system shall not enter system update mode in case of critical power,
            the system shall return an error code.
        \item The system shall stop all non-OS processes before updating software.
        \item The system shall update the primary application software.
    \end{enumerate}
\end{itemize}

\subsubsection{Restore Software From Backup}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        In case of an installation error the system must be able to restore the
        previous software version.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Software update failure.
            \item Response: The system restores the software backup.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall restore the software backup on software install failure.
        \item The system shall store a backup of the previous software version until
            a command is received to delete it.
    \end{enumerate}
\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%
% Universal to All Modes %
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Universal Requirements}
Universal requirements are requirements that affect all other modes.

\subsubsection{Transmitter Shutdown Command}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        FCC regulations require that all CubeSats with batteries be able to
        receive a transmitter shutdown command.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item Stimulus: Transmitter shutdown command received.
            \item Response: All outgoing transmissions are stopped until further instruction.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall be able to receive a transmitter shutdown command.
        \item The system shall authenticate the command.
        \item The system shall cease all outgoing transmissions.
        \item The system shall still passively receive data and commands.
    \end{enumerate}
\end{itemize}

\subsubsection{Transmitter Resume Command}
\begin{itemize}
    \item \textbf{Introduction and Puropose of Feature:}
        FCC regulations require that all CubeSats with batteries be able to
        receive a transmitter shutdown command, this feature should be
        reversible if the circumstances for transmission inhibit changes.
    \item \textbf{Stimulus/Response Sequence:}
        \begin{itemize}
            \item stimulus: Transmitter resume command received.
            \item Response: All outgoing transmissions are resumed.
        \end{itemize}
    \item \textbf{Associated Functional Requirements:}
    \begin{enumerate}
        \item The system shall be able to receive a transmitter resume command.
        \item The system shall authenticate the command.
        \item The system shall resume all outgoing transmissions.
    \end{enumerate}
\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Other non-functional requirements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Software System Attributes %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% There  are  a  number  of  attributes  of  software  that  can  serve  as
% requirements.  It  is  important  that  required attributes  be  specified
% so  that  their  achievement  can  be  objectively  verified.
\section{Software System Attributes}
\subsection{Reliability}
The system shall use static and dynamic analysis methods to determine
the lack of memory leaks.

\subsection{Security}
Command and control shall use authenticated encryption.

\subsection{Maintainability}
This mission if successful will be the start of an ODU space program, as such
wherever possible the code base should be designed with maintainability in mind.
Code shall be written to comply with any current standards, where there are newer
standards that are not used because they are immature or otherwise every effort
shall be given to ease transition for future revisions. Any exceptions to this
shall be documented along with the reason.

\subsection{Portability}
Future ODU space missions may use different hardware to accomplish their missions,
the most likely hardware changes would be between AVR and ARM Cortex-M4 
architectures. Wherever possible code shall be made portable between the two and
any exceptions shall be documented along with the reason.

\section{Other Requirements}
\subsection{Project Documentation}
The project shall use documentation generation in order to increase project
maintainability. Every module and function shall be documented with purpose,
parameters, output, and any relevant algorithms and/or equations.

\subsection{User Documentation}
User documentation shall be generated concerning all specific commands
that may be sent to the satellite and their error codes. The documentation
shall also provide references to any relevant OS or shell documentation.
The documentation shall assume no knowledge of CLI interfaces or programming.

\subsection{Code Style}
The project shall establish a code style guide that shall be followed
throughout the entire project.






\end{document}
